# mvc readme file




## Getting started


Hi and welcome, this repository contains the source code for a php-based web app that shows the
use of the MVC pattern. This corresponds to the MVC kick-off version.

Available online at: (coming soon)


The project has been created by using netbeans IDE, so, you can import it if you uses NB.


Code is organized as follows:

1. A model folder with a file responsible for management all DB conections and queries.

2. A controller folder containng a file in charge of coordinate the user requests and coordinate it with the model.

3. A view folder containing a file responsible for rendering the data from the model.

4. An app.json file for specifying the app features.


5. A file that contains a sqldump file from a MySQL DB (databasei_site.sql).


6. An index file invoking model and controller 


